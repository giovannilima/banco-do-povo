Cenário: Realizar dois depósitos numa conta nova

Dado que abrir uma conta corrente
Quando realizo um depósito de 300 reais
Então o saldo da minha conta será de 300 reais

Cenário: Realizar dois depósitos numa conta nova

Dado que abrir uma conta corrente
Quando realizo um depósito de 300 reais e 200 reais
Então o saldo da minha conta será de 500 reais