package br.ucsal.bes20182.testequalidade.banconpovo.teste;

import static org.junit.Assert.assertEquals;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import br.ucsal.bes20182.testequalidade.bancopovo.ContaCorrente;

public class ContaCorrenteSteps{

	private ContaCorrente contaCorrente;

	@Given("abrir uma conta corrente")
	public void abrirContaCorrente() {
		contaCorrente = new ContaCorrente();
	}

	@When("realizo um depósito de $deposito1 reais")
	public void realizarDeposito(Double deposito1) {
		contaCorrente.depositar(deposito1);
	}

	@Then("o saldo da minha conta será de $saldo")
	public void mostrarSaldoContaCorrente(Double saldo) {
		assertEquals(contaCorrente.consultarSaldo(), saldo);
	}

	@When("realizo um depósito de $deposito1 reais e $deposito2 reais")
	public void realizarDoisDepositos(Double deposito1, Double deposito2) {
		contaCorrente.depositar(deposito1);
		contaCorrente.depositar(deposito2);
	}

}
